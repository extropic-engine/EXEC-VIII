﻿using System;
using CocosSharp;

namespace EXECVIII
{
	public class ExecAppDelegate : CCApplicationDelegate
	{
		public override void ApplicationDidFinishLaunching(CCApplication application, CCWindow mainWindow)
		{
			application.PreferMultiSampling = false;
			application.ContentRootDirectory = "Content";

			var bounds = mainWindow.WindowSizeInPixels;
			CCScene.SetDefaultDesignResolution (bounds.Width, bounds.Height, CCSceneResolutionPolicy.ShowAll);

			ExecScene scene = new ExecScene (mainWindow);
			mainWindow.RunWithScene (scene);
		}

		public override void ApplicationDidEnterBackground (CCApplication application)
		{
		}

		public override void ApplicationWillEnterForeground (CCApplication application)
		{
		}
	}
}

